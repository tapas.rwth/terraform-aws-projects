#generate secure public key on the fly
resource "tls_private_key" "generatekey" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

#ecodes into PEM and saved in local file, it will be needed to login into ec2 instance
resource "local_file""save_pem"{
    content = tls_private_key.generatekey.private_key_pem
    filename = "${path.module}/${var.ami-key-pair-name}.pem"
    
}

#create key-pair in aws to login into ec2 instance using previously generated key
resource "aws_key_pair" "loginkey" {
  key_name   = "login-key"
  #public_key = file("${path.module}/login-key.pem.pub")
  public_key = tls_private_key.generatekey.public_key_openssh
}

#change the permission of the .pem so that root user can use it
resource "null_resource""sh_command"{
  provisioner "local-exec"{  
      command = "chmod 400 ${local_file.save_pem.filename}"
    
    }
}
