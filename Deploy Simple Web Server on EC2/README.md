**Deploy Simple Web Server on EC2 instance**

**Project contains follwoing .tf file:**
    
- ec2.tf : contains configuration to create ec2 instance, access key id and secret key is configured in AWS CLI so these are not given in provider section of the resources    
- ec2variables.tf : variables used in different resources     
- loginkeypair.tf : generate public/private key and create key-pair in aws, this key pair is used to ssh login into deployed ec2    
- securitygroup.tf : create security group    
- sg.tf : same as securitygroup.tf (not used, commented)
- webserver.sh: (bootstrap script passed as user data)

------------------------------------------------------------------------------------------------------------------------------------

**EC2 Instance is deployed in aws**



![small](./image/ec2_instance.PNG)

------------------------------------------------------------------------------------------------------------------------------------

**security group**



![small](./image/sg.PNG)
------------------------------------------------------------------------------------------------------------------------------------

**deployed app**



![small](./image/app.PNG)
------------------------------------------------------------------------------------------------------------------------------------

**ssh connection to the ec2 using generated login-key pair**



![small](./image/sshtoec2.PNG)
------------------------------------------------------------------------------------------------------------------------------------
