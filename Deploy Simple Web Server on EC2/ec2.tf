#terraform config
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.10.0"
    }
  }
}

#provider
provider "aws" {
  profile = "default"
  region  = "eu-central-1"
}

#fetch the ami id in the desired region
data "aws_ami" "getami" {
  owners      = ["amazon"]
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }

}

#create aws ec2 instance
resource "aws_instance" "my-ec2" {
  ami           = data.aws_ami.getami.id
  instance_type = var.instancetype
  #key_name = var.ami-key-pair-name
  key_name               = aws_key_pair.loginkey.key_name
  vpc_security_group_ids = [aws_security_group.sg_terraform.id]
  tags = {
    Name = "test-tj-webserver"
  }
  availability_zone = var.az
  user_data         = file("${path.module}/webserver.sh")

}

#produce ec2 instance public ip address in the output
output "instanceip" {
  value = aws_instance.my-ec2.public_ip
}