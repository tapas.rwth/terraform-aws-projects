data "aws_vpc" "default" {
  default = true
}


#create security group
resource "aws_security_group" "sg_terraform" {
  name        = "sg_terraform"
  description = "Ingress for web service"
  vpc_id      = data.aws_vpc.default.id

#iterate over port numbers from port list
  dynamic "ingress" {
    for_each = var.sg_ports
    iterator = port
    content {
      from_port   = port.value
      to_port     = port.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
  
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "sg_terraform"
  }

}